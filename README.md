# python-playsound

Pure Python library for playing sounds

https://github.com/TaylorSMarks/playsound

<br>

How to clone this repository:

```
git clone https://gitlab.com/azul4/content/multimedia/tartube/python-playsound.git
```

